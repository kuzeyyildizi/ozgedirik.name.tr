# Masal-1  
  
ben;  
baskınlardan kaçıp  
evine sığınan bir babanın  
sevdiğine attığı küçük bir imzayım.  
  
kurşunların taahhütlü gönderildiği günlerde  
cumhuriyet gazetesinin üzerine doğmuşum.  
kıçımda büyük puntolarla seksen ihtilalinin izi  
acıyor hâlâ yediğim ilk ve son iğnenin yeri.  
  
göğüslerinde hapşurunca ben  
dayanamayıp süt tanrıçam, ihbar etmiş babamı  
sağcı kestanelerin göbekleri çatlarken gülmekten  
çıra gibi tutuşmuş babamın kitapları.  
  
iki bacağımın arasından, tersten bakıp  
misafir beklemişim  
sahilde balonlar yürüyüş yapıp  
saçmalı tüfekleriyle patlatmaya çalışırken ipe dizilmiş insanları  
geldi gelecek kardeşimi annemin içine ittirmişim.  
  
hep güzel, hep hayal kalsın diye  
açmış annem kumbaramı  
bir kilo erişte, biraz buğday için  
yapmazmış bunu  
  
kapının dibinde  
örümceğe imrenerek geçerken  
657’ye tabi ipekböcekleri  
“bir ihtilal daha var” deyip ölen babamın  
yanındaki çukura  
söz vermişim.

---
Kaynak:

- Dirik, Özge: “Masal-1”, **Kuzey Yıldızı**, Mart-Nisan 2005, Sayı 11, s. 17.
