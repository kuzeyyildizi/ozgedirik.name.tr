# Satır-a Temenniler  
  
gizlemeye çalıştığınız satırlarınız  
ezberlemiştir kanın rengini  
ama düşünmez değil mi,  
artık kanmayacak bir yazarı.  
  
yaşamak, ol’madığından emer sütünü  
yazarken düşünmek budur oysa  
yazarken düşürülmek  
zincirleme pusularla.  
  
tırnak içine alıp, hatırlattığınız  
güzel bir mısrası olsaydı Arzu’nun  
nasıl barışırdık o zaman bir bilseniz  
isra(f )il bile çıldırır,  
göçerdi aramızdan...

---
Kaynak:

- Dirik, Özge: “Satır-a Temenniler”, **Kuzey Yıldızı**, Mart-Nisan 2005, Sayı 11, s. 10.
