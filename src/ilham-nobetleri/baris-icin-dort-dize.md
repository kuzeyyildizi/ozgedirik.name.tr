# Barış İçin Dört Dize

kafalarınızı kumdan çıkarıp  
namlularınıza karanfil sokun  
tek ayağıyla sek sek oynayan  
Asyalı çocuğun kırmayın umudunu

---
Kaynak:

- Dirik, Özge: “Barış İçin Dört Dize”, **Kuzey Yıldızı**, Mart-Nisan 2003, Sayı 7, s. 17.
