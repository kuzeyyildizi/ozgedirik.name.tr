# Ba Ba  
  
veysel çişini söylemeyi öğrendi  
memurum  
maaşıma zam demek bu.  
  
böyle utanmamıştım  
iki artı bir olalı  
“en sevdiğin yemek ne” deyince anneannesi  
gülerek yanıtladı Veysel;  
kahvaltı.

---
Kaynak:

- Dirik, Özge: “Ba Ba”, **Kuzey Yıldızı**, Mart-Nisan 2005, Sayı 11, s. 17.
