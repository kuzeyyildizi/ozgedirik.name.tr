# Şiire Küsenler Antolojisi  
  
hatırlamayın beni  
acı veriyor unuttuğunuzu bilmek  
küçük hesaplar koştu peşimden  
kira, şarap, sigara  
şimdi bir ayağım daha kısa.  
  
dostlar şiire küsülmez diyor  
aşka küsülür şiire küsülmezmiş  
palavra.  
toprağa bile küsülür kefeni sıvazlarsa  
intihara bile  
çocuğuna bile  
küsülür oyaladığın aynaya  
Oya-ladığın hayatıma bile...

---
Kaynak: 

- Dirik, Özge: "[Özge Dirik Arşivi: Şiirler](https://kuzeyyildizi.com/files/ozgedirik-siirler.pdf)", **Kuzey Yıldızı Edebiyat Dergisi**, Yayım Tarihi: 11.04.2014, [https://kuzeyyildizi.com/pdf](https://kuzeyyildizi.com/pdf).
