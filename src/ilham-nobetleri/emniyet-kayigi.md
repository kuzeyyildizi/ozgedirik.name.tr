# Emniyet Kayığı

yalnızca hüznünde anlıyorsun yüzümü,  
ve arkasında çelimsiz, yitik bir kayık götürüyor,  
kendine güvenen, güzel bir gemi...

---
Kaynak:

- Dirik, Özge: “Emniyet Kayığı”, **Kuzey Yıldızı**, Mart-Nisan 2005, Sayı 11, s. 10.
