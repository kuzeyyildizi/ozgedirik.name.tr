# Kalabalık  
  
> “ipekböceği attım  
> eşarp düştü içime...”  
  
uyandım  
rüyamda kanamış dilim  
belki kıtlama jiletle bağrılan  
yaşamöyküleri anlatmışımdır çocuklara.  
çocuklar dedim de  
onlar da kanadılar  
kanınca bana.  
  
kalktım  
bir eşkıya rica etti yüklerimi  
güzel de bir kadın  
çocuğunu öleceği yaşa büyütemeden giden  
bir anneyi uğurlamış olsa da  
on yedi kalp kriziyle  
yürüdüm  
adımlarım nasıl da uyarılıyor  
kapıyı çalan biri olduğunda  
isterse bir hırsız olsun  
kapıyı çalmaya yeltenen  
  
öldüm  
  
ve yarın üşüştüler başıma; yaşlar, ayaklar, gözler  
ve yarı yaşam yakınmaları sürdü adıma  
ve yar uzun saçlı bir adamla geldi mezarlığa  
ve ya bir kadınla...  
  
ve  
  
gömdüler beni,  
öldürdükleri gibi  
özenle.

---
Kaynaklar:

- Dirik, Özge: “Kalabalık”, **Hece**, Eylül 2004, Sayı 93.
- Dirik, Özge: “Kalabalık”, **Kuzey Yıldızı**, Mart-Nisan 2005, Sayı 11, s. 9.
