# Çirkin Ördek Derisi(nden Eldiven İmal Edenler)  
  
kötü huylu bir kist dünya  
tanrının bedeninde.  
  
“ok!”  
sana rekâtlanmış yüzlerce dize.  
  
içimi kaşıyan bekleyişte  
büyütünce bir şiir  
şiiri de küstürdüm  
büyütüp.  
  
gördüm  
ölü göze ait son kareleri  
küçüktüm  
siyah beyaz siyah beyaz  
durdu siyahta.  
  
on yedi kalp krizi  
gücü kırılır elbet  
bahsi geçen bir anne  
çocuğunu öleceği yaşa  
büyütemediğinden  
dirense de.  
  
elmaya dünya düştü,  
sen de anlardın  
iğnen deli bilindi mi  
buğulanır pusulanın camı..  

---
Kaynak:

- Dirik, Özge: “Çirkin Ördek Derisi(nden Eldiven İmal Edenler)”, **Kuzey Yıldızı**, Mart-
Nisan 2005, Sayı 11, s. 20.
