# İlham Nöbetleri  
  
kırarsın bazen ekmeği  
öyle buğu falan da çıkmaz  
bayattır  
ya da  
ısıtılmıştır bir bayatlığın üzerine  
ama masana doluverir  
ilham perileri  
  
masanın altında  
açlıktan ayağına göz koymuşlar  
kalemini oynatmaya başladın ya  
hemen kıskanır  
ilham kedileri  
  
biraz içeri gir  
dil ovasının altında binlerce şair  
–mezara nasıl da yakışıyorlar  
yaşarken kemirilen cesetler–  
onlara gülüyorlar  
ilham pireleri  
  
öpüştükten sonra ağzımda  
ispirto tadı bırakan kadınlar  
girer rüyalarıma  
ama öyle değil  
ne kadar küçülürse küçülsünler  
mide bulandırmıyor  
ilham sinekleri...

---
Kaynak:

- Dirik, Özge: “İlham Nöbetleri”, **Kuzey Yıldızı**, Mart-Nisan 2005, Sayı 11, s. 22.
