# ikigen

işaret parmağınızdaki  
intihar eğilimli çirkin yüzük  
zehir almaya geldi dün, bana.

hiç dokunmadan anlattım  
aşk;  
takip mesafesini koruyamayanlara  
tanrının verdiği ceza.

kadehimden dudaklarınızı çekseydiniz  
mantarımı içine düşürmezdim  
dünyanızın.

hiçbir göz yörüngesine yetmez.  
ne sidik, ne polis  
yağmur söktü afişlerimizi  
tabağımıza yemek  
midemize esaret koydular.

vücudunuzda,  
siyahı marifet sanan benleriniz  
çikolata damlalarına dönüşebilir bu gece  
yastığınızdaki rimel lekesini  
zor yazılmış bir mektuba sayarım,  
gidince.

---
Kaynak:

- Dirik, Özge: “[İkigen](https://kuzeyyildizi.com/sites/default/files/ky11.pdf)”,  **Kuzey Yıldızı Edebiyat Dergisi**, Mart-Nisan 2005, Sayı 11, s. 11.
