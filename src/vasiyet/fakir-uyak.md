# fakir uyak

ölümden önceki uyak  
yaşamak adına ağzımdan kaçırdığım kuşlar,  
kim bilir şanslarını kimin üzerine pisliyor.

ölümden önceki dudak  
–suratın sırat olsa  
geçemezdim gözlerinden  
kaç kan aksa– ile tavladığım kadın  
kim bilir hangi efendinin valsinde tırnak yiyor.

ölümden önceki tuzak  
traji-kolik hayatımın tirajı komik öyküleri  
süs arıyor bir yanım intiharlarıma  
cinayet süsü.

ölümden önceki uyak  
konaklaması bir ipte iki cambazın  
sevişerek mümkün ancak...

---
Kaynak:

- Dirik, Özge: “[Fakir Uyak](https://kuzeyyildizi.com/sites/default/files/ky11.pdf)”, **Kuzey Yıldızı Edebiyat Dergisi**, Mart-Nisan 2005, Sayı 11, s. 10.
