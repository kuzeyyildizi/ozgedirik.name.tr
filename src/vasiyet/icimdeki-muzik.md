# içimdeki müzik

bam telimde parmak izin duruyor  
yeni boyanmış bir aşka oturduk  
kalkarsak üzerimizde kalacak izi

korsan limanlarda bekliyoruz birbirimizi  
omzumuzda mırıldanan güvercinler dahil  
aldatıyor bu kahperengi hayat bizi

sarhoş olup zehirliyoruz sırlarını  
bu aşkı herkese susmak  
şarapsız çalmam kadar ayıp kapını

içimdeki müziğin susması  
altındaki tabureyi tekmeleyip kemancının  
çalması gibi son notalarını...

---
Kaynak:

- Dirik, Özge: “[İçimdeki Müzik](https://kuzeyyildizi.com/sites/default/files/ky11.pdf)”, **Kuzey Yıldızı Edebiyat Dergisi**, Mart-Nisan 2005, Sayı 11, s. 10
