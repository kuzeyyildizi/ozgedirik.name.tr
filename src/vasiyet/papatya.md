# papatya

Zamansızlığımdandır güzelliğim.  
Yol kenarını mesken tutan papatyalar,  
kurtaramazlar canlarını,  
dikkatli çocukların tutkularından.  

Bütün yapraklarım “sevmiyor” diye,  
ucuz bir hediye olamam gerçi,  
ama bilinir ki;  
ne zaman bir çiçek dalında kurusa,  
bir sevgili daha çok üzülür.  

Yüzünü görünce onun,  
ne de çok isterdim incinmesin.  
Benden önce sen ispiyonlasaydın keşke  
başka bir adama harcadığın sevgini.  

Kırmızıyı esirgemeyen çay bardaklarının  
ince bellerine dayanamadan,  
beni de aldatıyordur belki,  
sevinince terleyen parmakların.  

_Şubat 2002_

---
Kaynaklar:

- Dirik, Özge: “[Papatya](https://kuzeyyildizi.com/sites/default/files/ky04.pdf)”, **Kuzey Yıldızı Edebiyat Dergisi**, Ağustos-Eylül 2002, Sayı 4, s. 16
