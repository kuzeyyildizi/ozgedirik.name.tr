# bor’un pazarı

Bir şiirle bitirelim bugün;

BOR’UN PAZARI

aynı kadınla ikinci defa evlenmek,  
ikinci defa yakılan sigaranın ağır tadı,  
ha bitti, ha bitecek...

aynı kadınla üçüncü defa evlenmek,  
denizin demlediği vapur çayı  
ama pahalı.

aynı kadınla dördüncü defa evlenmek,  
bohçacılar topluyorlar kalanları,  
ısrarkâr geçimsizlikten hoşnut şehrin baro’su.

aynı kadınla beşinci defa evlenme.

---
Kaynak:

- Dirik, Özge: “Bor’un Pazarı”, **Kuzey Yıldızı**, Mart-Nisan 2005, Sayı 11, s. 10.
