# ikincil ruhla pis-duvar buluşmaları

on iki sandalyeli bir masayla, masanın gençliğinden konuşuyorduk.  
on bir sandalye ve iki intihar büyütmüş balkon pür dikkat beni  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dinliyorlardı.

zamanın mücadelesi armağan etmişti bizi, birbirimize.  
pireli bir devletin kanatlarının arasındaki karıncalardık.  
ne söylesek ayıptı biraz söylemesi.

dahası an, tıbben ölüydü.  
atık kamyonlarında mühürlü bir yürek  
şehir çöplüğünde martı ziyafetinden önce  
bir film setine emanet edilirdi belki,  
korkuturdu yine bizi.

senin dünyanda vapur kalkınca  
balıklar çamaşır yıkardı  
içindeki hileli sayaçların aritmetiği  
sıfırdan sıkılmıyordu bir türlü

tırabzanlardan aşağıya  
ayaklarını sallandırıp  
annesine hınzır hınzır gülen o çocuk  
uçurumlara gözlerini gıdıklatacak yaşa çoktan geldi.  
ama ikimiz de biliyorduk  
elleri harita kadar acılı her annenin son görevi  
çocuğunu öleceği yaşa büyütmekti.

sağır ve dilsizler ülkesinde  
kulaktan kulağa oynarken özgürlük düşün,  
sigaranla aynıydı aşkının geleceği  
duman hali.

şimdi biz,  
yatırılmamış bir şans kuponu  
pişmanlık olur en iyi ihtimalimiz.

oysa  
mendil satar yine de bakardım bu kente  
olsaydın içinde.

_ist(a)kozyatağı_

---
Kaynak:

- Dirik, Özge: “İkincil Ruhla Pis-duvar Buluşmaları”, Kuzey Yıldızı, Temmuz-Ağustos 2006, Sayı 13, s. 4.
